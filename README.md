# Inkscape Templates

It's described in full on the label. We're about transparency here. And buzzwords.

### Installation

##### Linux

Copy files to `~/.config/inkscape/templates`

##### Windows

Copy files to `%appdata%\inkscape\templates\`
